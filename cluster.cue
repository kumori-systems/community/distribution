//  Copyright 2022 Kumori Systems S.L.
//
//  Licensed under the EUPL, Version 1.2 or – as soon they
//  will be approved by the European Commission - subsequent
//  versions of the EUPL (the "Licence");
//  You may not use this work except in compliance with the
//  Licence.
//  You may obtain a copy of the Licence at:
//
//  https://joinup.ec.europa.eu/software/page/eupl
//
//  Unless required by applicable law or agreed to in
//  writing, software distributed under the Licence is
//  distributed on an "AS IS" basis,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
//  express or implied.
//  See the Licence for the specific language governing
//  permissions and limitations under the Licence.

//
// This is just an example! Set your enterprise values
//
package clustermanager

import k "kumori.systems/cluster/kdv"

node1: {
  ip: "172.16.100.11"
  labels: [{noderole: "master"}]
}
node2: {
  ip: "172.16.100.12"
  labels: [{noderole: "worker"}]
}
node3: {
  ip: "172.16.100.13"
  labels: [{noderole: "worker"}]
}

masterNodes: [node1]
workerNodes: [node2, node3]

ingressNodes: [node2, node3]
storageNodes: [node2, node3]
topology: masterNodes + workerNodes

// // If custom availability zones are used. By default, each node is in a different availability zone.
// // Zones must be also added below in the "cluster" section (see below)
// zonesConfig: zone1: nodes: [node1, node2]
// zonesConfig: zone2: nodes: [node3]

sshAccess: {
  user: "ubuntu"
  privateKeyPath: "${HOME}/.ssh/my_private_key"
}

clusterPkiConfig: {
  rootCaFile: "${HOME}/my_root_ca.crt"
}

dnsConfig: {
  referenceDomain: "mydomain.cloud"
  referenceDomainCertDir: "${HOME}/mydomain-cloud-certificates"
  management: {
    enabled: true
    provider: {
      name: "route53"
      config: {
        configDir: "${HOME}/my_aws_config"
      }
    }
  }
}

ingressConfig: {
  nodes: ingressNodes
  internalIP: "10.0.0.101"
  externalIP: "52.16.195.171"
  tcpPorts: []
  certHeaderFormat: "PEM"
}

apiServerConfig: {
  internalIP: "10.0.0.100"
  externalIP: "52.16.195.170"
}

storageConfig: {
  nodes: storageNodes
}

customScriptsConfig: {
  enabled: false
  directory: "${HOME}/my_custom_scripts"
}

dockerHubCredentialsConfig: {
  username: "username2"
  password: "password2"
}

cluster: k.#Cluster & {
  name                 : "mycluster"
  machines             : topology
  ssh                  : sshAccess
  dockerBridgeNet      : "172.16.0.1/16"
  podNet               : "172.17.0.0/16"
  clusterIPNet         : "172.18.0.0/16"
  internalDomain       : "cluster.local"
  dns                  : dnsConfig
  clusterPki           : clusterPkiConfig
  ingress              : ingressConfig
  apiServer            : apiServerConfig
  storage              : storageConfig
  customScripts        : customScriptsConfig
  dnsResolvers         : []
  dockerHubCredentials : dockerHubCredentialsConfig
  registryMirrors      : []
  // zonesConfig          : zones   // If custom availability zones are used
}

c:cluster
