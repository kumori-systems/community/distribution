//  Copyright 2022 Kumori Systems S.L.
//
//  Licensed under the EUPL, Version 1.2 or – as soon they
//  will be approved by the European Commission - subsequent
//  versions of the EUPL (the "Licence");
//  You may not use this work except in compliance with the
//  Licence.
//  You may obtain a copy of the Licence at:
//
//  https://joinup.ec.europa.eu/software/page/eupl
//
//  Unless required by applicable law or agreed to in
//  writing, software distributed under the Licence is
//  distributed on an "AS IS" basis,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
//  express or implied.
//  See the Licence for the specific language governing
//  permissions and limitations under the Licence.

package clustermanager

config: {
  keycloak: {
    enabled: false
    adminUsername: "username"
    adminPassword: "password"
    resources: {
      cpu: "1000m"
      cpuLimit: "1500m"
      mem: "700Mi"
      memLimit: "1500Mi"
    }
  }
  ambassador: {
    resources: {
      cpu: "250m"
      cpuLimit: "1000m"
      mem: "300Mi"
      memLimit: "800Mi"
    }
  }
  "kube-prometheus": {
    enabled: true
    prometheus: {
      retentionTime: "2d"
      resources: {
        cpu: "300m"
        cpuLimit: "1000m"
        mem: "500Mi"
        memLimit: "1500Mi"
      }
    }
    configreloader: {
      resources: {
        cpu: "100m"
        cpuLimit: "100m"
        mem: "50Mi"
        memLimit: "50Mi"
      }
    }
    alertmanager: {
      resources: {
        cpu: "100m"
        cpuLimit: "200m"
        mem: "100Mi"
        memLimit: "500Mi"
      }
    }
    grafana: {
      adminUsername: "username"
      adminPassword: "password"
      viewerUsername: "viewer"
      viewerPassword: "viewer"
      strictRoles: true
      resources: {
        cpu: "100m"
        cpuLimit: "200m"
        mem: "200Mi"
        memLimit: "400Mi"
      }
    }
    cadvisor: {
      enabled: true
      resources: {
        cpu: "400m"
        cpuLimit: "800m"
        mem: "400Mi"
        memLimit: "2000Mi"
      }
    }
  }
  "kumori-admission": {
    authenticationType: "clientcertificate"
    clientCertAuthParams: {
      clientCertRequired: true
      clientCertValidateWithTrustedCA: false
      serverCertDir: "${HOME}/admission-server-cert"
    }
  }
}
